package com.wgs.hibernate;

import org.hibernate.Session;

import com.wgs.hibernate.domain.Account;

/**
 * Hello world!
 * 
 */
public class App {
  public static void main(String[] args) {
    System.out.println("Hello World!");
    
    Session session = HibernateUtil.getSessionFactory().openSession();
    session.beginTransaction();
    
    Account account = new Account();
    account.setUsername("agus.zulvani@wgs.co.id");
    account.setPassword("rahasiaDunk");
    
    session.save(account);
    session.getTransaction().commit();
    
    System.out.println("Success");
  }
}
